#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygtk
import gtk
import random

class Matematicando():

	def __init__(self):
		self.Window_one = gtk.Window()
		self.Window_one.set_title("Matematicando")
		self.Vbox_one = gtk.VBox()
		self.Window_one.add(self.Vbox_one)
		self.Label1 = gtk.Label("Inserisci un Numero: ")
		self.Entry1 = gtk.Entry()
		self.Entry1.connect("activate", self.Tabelline)
		self.Button1 = gtk.Button("Calcola")
		self.Button1.connect("clicked", self.Tabelline)
		self.Button2 = gtk.Button("Indovina")
		self.Button2.connect("clicked", self.Esercizio)
		self.a = gtk.Label()
		self.b = gtk.Label()
		self.c = gtk.Label()
		self.d = gtk.Label()
		self.e = gtk.Label()
		self.f = gtk.Label()
		self.g = gtk.Label()
		self.h = gtk.Label()
		self.i = gtk.Label()
		self.l = gtk.Label()
		self.m = gtk.Label()
		self.n = gtk.Label()
		self.Vbox_one.pack_start(self.Label1)
		self.Vbox_one.pack_start(self.Entry1)
		self.Vbox_one.pack_start(self.Button1)
		self.Vbox_one.pack_start(self.Button2)
		self.Vbox_one.pack_start(self.a)
		self.Vbox_one.pack_start(self.b)
		self.Vbox_one.pack_start(self.c)
		self.Vbox_one.pack_start(self.d)
		self.Vbox_one.pack_start(self.e)
		self.Vbox_one.pack_start(self.f)
		self.Vbox_one.pack_start(self.g)
		self.Vbox_one.pack_start(self.h)
		self.Vbox_one.pack_start(self.i)
		self.Vbox_one.pack_start(self.l)
		self.Vbox_one.pack_start(self.m)
		self.Vbox_one.pack_start(self.n)
		self.Window_one.show_all()
	def Tabelline(self, Entry1):
		self.Numero = int(self.Entry1.get_text())
		self.Result = self.Numero*1
		self.a.set_text(str(self.Numero)+" x 1 = " +str(self.Result))
		self.Result = self.Numero*2
		self.b.set_text(str(self.Numero)+" x 2 = "+str(self.Result))
		self.Result = self.Numero*3
		self.c.set_text(str(self.Numero)+" x 3 = "+str(self.Result))
		self.Result = self.Numero*4
		self.d.set_text(str(self.Numero)+" x 4 = "+str(self.Result))
		self.Result = self.Numero*5
		self.e.set_text(str(self.Numero)+" x 5 = "+str(self.Result))
		self.Result = self.Numero*6
		self.f.set_text(str(self.Numero)+" x 6 = "+str(self.Result))
		self.Result = self.Numero*7
		self.g.set_text(str(self.Numero)+" x 7 = "+str(self.Result))
		self.Result = self.Numero*8
		self.h.set_text(str(self.Numero)+" x 8 = "+str(self.Result))
		self.Result = self.Numero*9
		self.i.set_text(str(self.Numero)+" x 9 = "+str(self.Result))
		self.Result = self.Numero*10
		self.l.set_text(str(self.Numero)+" x 10 = "+str(self.Result))
		self.Result = self.Numero*11
		self.m.set_text(str(self.Numero)+" x 11 = "+str(self.Result))
		self.Result = self.Numero*12
		self.n.set_text(str(self.Numero)+" x 12 = "+str(self.Result))
	def Esercizio(self, Button2):
		self.Window_two = gtk.Window()
		self.Vbox_two = gtk.VBox()
		self.Window_two.add(self.Vbox_two)
		self.Label2 = gtk.Label()
		self.Label3 = gtk.Label("Dai Rispondi")
		self.Entry2 = gtk.Entry()
		self.Entry2.connect("activate", self.Invia)
		self.Button3 = gtk.Button("Avvia")
		self.Button4 = gtk.Button("Invia")
		self.Button3.connect("clicked", self.Avvia)
		self.Button4.connect("clicked", self.Invia)
		self.Vbox_two.pack_start(self.Label2)
		self.Vbox_two.pack_start(self.Entry2)
		self.Vbox_two.pack_start(self.Label3)
		self.Vbox_two.pack_start(self.Button3)
		self.Vbox_two.pack_start(self.Button4)
		self.Window_two.show_all()
		gtk.main()
	def Avvia(self, Label2):
		self.Button3.set_label('Ricarica')
		self.Label3.set_text('Dai Rispondi')
		self.z = random.randrange(1, 11)
		self.w = random.randrange(1, 13)
		self.k = int(self.z)*int(self.w)
		self.Label2.set_text(str(self.z)+" x "+str(self.w)+" = ")
	def Invia(self, Entry2):
		self.Answer = int(self.Entry2.get_text()) 	
		if self.Answer == self.k:
			self.Label3.set_text('Esatto!')
		else:
			self.Label3.set_text('Sbagliato!')
		
	
	def main(self):
		gtk.main()
		
if __name__ == "__main__":
	matematicando = Matematicando()
	matematicando.main()
	
