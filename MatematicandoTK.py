#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Tkinter import*
import random, sys



class Matematicando():

	def Tabelline(self):
		self.x = self.Variable_one.get()
		self.y = self.x*1
		self.a['text'] = str(self.x)+" x "+ "1" + " = "+str(self.y)
		self.y = int(self.x)*2
		self.b['text'] = str(self.x)+" x "+ "2" + " = "+str(self.y)
		self.y = int(self.x)*3
		self.c['text'] = str(self.x)+" x "+ "3" + " = "+str(self.y)
		self.y = int(self.x)*4
		self.d['text'] = str(self.x)+" x "+ "4" + " = "+str(self.y)
		self.y = int(self.x)*5
		self.e['text'] = str(self.x)+" x "+ "5" + " = "+str(self.y)
		self.y = int(self.x)*6
		self.f['text'] = str(self.x)+" x "+ "6" + " = "+str(self.y)
		self.y = int(self.x)*7
		self.g['text'] = str(self.x)+" x "+ "7" + " = "+str(self.y)
		self.y = int(self.x)*8
		self.h['text'] = str(self.x)+" x "+ "8" + " = "+str(self.y)
		self.y = int(self.x)*9
		self.i['text'] = str(self.x)+" x "+ "9" + " = "+str(self.y)
		self.y = int(self.x)*10
		self.l['text'] = str(self.x)+" x "+ "10" + " = "+str(self.y)
		self.y = int(self.x)*11
		self.m['text'] = str(self.x)+" x "+ "11" + " = "+str(self.y)
		self.y = int(self.x)*12
		self.n['text'] = str(self.x)+" x "+ "12" + " = "+str(self.y)
	def Start(self):
		self.StarterButton['text'] ="Ricarica!"
		self.Moltiplicando = random.randrange(1, 11)
		self.Moltiplicatore = random.randrange(1, 13)
		self.Prodotto = int(self.Moltiplicando)*int(self.Moltiplicatore)
		self.Domanda['text'] = str(self.Moltiplicando)+" x "+str(self.Moltiplicatore)+" = "
	def Convalida(self):
		self.Risposta_get = self.Risposta.get()
		self.Risposta_int = int(self.Risposta_get)
		if self.Risposta_int == self.Prodotto:
			self.Verdetto['text'] = "Esatto!"
		else:
			self.Verdetto['text'] = "Sbagliato!"
	def Esercizio(self):
		self.Window_two = Tk()
		self.Window_two.resizable(False, False)
		self.Window_two.geometry("200x100")
		self.Window_two.title('Matematicando')
		self.Frame_one = Frame(self.Window_two)
		self.Frame_one.pack(side = "top")
		self.Domanda = Label(self.Frame_one, text = "")
		self.Domanda.pack(side = "left")
		self.Risposta = Entry(self.Frame_one, width = "6")
		self.Risposta.pack(side = "right")
		self.Risposta.bind("<Return>", self.Convalida)
		self.Frame_two = Frame(self.Window_two)
		self.Frame_two.pack()
		self.Verdetto = Label(self.Frame_two)
		self.Verdetto.pack()
		self.Frame_three = Frame(self.Window_two)
		self.Frame_three.pack(side = "bottom")
		self.StarterButton = Button(self.Frame_three, text = "Avvia!")
		self.StarterButton.pack(side = "left")
		self.GoodButton = Button(self.Frame_three, text = "Invia!")
		self.GoodButton.pack(side = "left")
		self.GoodButton['command'] = self.Convalida
		self.StarterButton['command'] = self.Start
		self.Window_two.mainloop()



	

	def __init__(self, root):
		self.Frame_one = Frame(root) # First Frame
		self.Frame_one.pack() # I pack the Frame
		Label(self.Frame_one, text = "Inserisci un numero:").pack(side = "top") # Sample Label 
		self.Variable_one = Entry(self.Frame_one) # This Variable contain the number to multiply
		self.Variable_one.pack(side = "top") # I pack the "Entry" and choice the side(top)
		self.Variable_one.bind("<Return>", self.Tabelline)
		self.Variable_one.focus()
		self.Button_one = Button(self.Frame_one, text = "Calcola", width = "17")
		self.Button_one['command'] = self.Tabelline
		self.Button_one.pack(side = "top")
		self.Button_two = Button(self.Frame_one, text = "Prova ad indovinare", width = "17", command = self.Esercizio)
		self.Button_two.pack(side = "top")
		self.a = Label(self.Frame_one, text = "")
		self.a.pack(side = "top")
		self.b = Label(self.Frame_one, text = "")
		self.b.pack(side = "top")
		self.c = Label(self.Frame_one, text = "")
		self.c.pack(side = "top")
		self.d = Label(self.Frame_one, text = "")
		self.d.pack(side = "top")
		self.e = Label(self.Frame_one, text = "")
		self.e.pack(side = "top")
		self.f = Label(self.Frame_one, text = "")
		self.f.pack(side = "top")
		self.g = Label(self.Frame_one, text = "")
		self.g.pack(side = "top")
		self.h = Label(self.Frame_one, text = "")
		self.h.pack(side = "top")
		self.i = Label(self.Frame_one, text = "")
		self.i.pack(side = "top")
		self.l = Label(self.Frame_one, text = "")
		self.l.pack(side = "top")
		self.m = Label(self.Frame_one, text = "")
		self.m.pack(side = "top")
		self.n = Label(self.Frame_one, text = "")
		self.n.pack(side = "top")
#Fine Label
		

Window_one = Tk()
Window_one.resizable(False, False)
Window_one.title('Matematicando')
matematicando = Matematicando(Window_one)
Window_one.mainloop()
		
		
